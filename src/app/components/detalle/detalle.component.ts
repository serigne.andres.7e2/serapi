import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { DataLocalService } from '../../services/datalocal.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {
  @Input() id
  drink: any;

  constructor(private modalCtrl: ModalController, private data: DataService, private localData: DataLocalService) { }

  ngOnInit() {
    this.data.getDrinksDetalle(this.id).subscribe(
      resp=> {
        this.drink = resp.drinks;
        console.log(this.drink)
      }
    )
  }
  regresar(){
    this.modalCtrl.dismiss()
  }
 
  favorito(bebida){
    this.localData.addBebida(bebida)
  }
}