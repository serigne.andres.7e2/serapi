import { Component, Input } from '@angular/core';
import { DataService } from '../services/data.service';
import { Drink } from '../interficies/interficies';
import { DetalleComponent } from '../components/detalle/detalle.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  @Input() id;
  alcoholicas : Drink[]=[];
  letras: any;

  constructor(private data: DataService, private modalCtrl: ModalController) {}

  ngOnInit(): void{
    this.infoAlcoholicas();
  }

  infoAlcoholicas(event?){
    this.data.getAlcoholicas().subscribe(resp =>{
        this.alcoholicas.push(...resp.drinks);
        this.letras = this.alcoholicas;
        if(event){
          event.target.complete();
        }
    })
  }

  loadData(event){
    this.infoAlcoholicas(event);
  }
  async verDetalle(id: string){
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: { id }
    })
    modal.present()
  }
 
  busca(nombre){
    if(nombre != ""){
      this.letras = this.alcoholicas.filter(n => n.strDrink.toLowerCase().indexOf(nombre.toLowerCase())> -1);
    }
    else{
      this.letras = this.alcoholicas;
    }
  }
}
