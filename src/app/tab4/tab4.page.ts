import { Component, OnInit } from '@angular/core';
import { Drink } from '../interficies/interficies';
import { DataLocalService } from '../services/datalocal.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  alcoholicas : Drink[]=[];


  constructor(private data: DataLocalService) { }

  async ngOnInit() {
    this.alcoholicas = await this.data.getData();

  }
   async ionViewWillEnter(){
     this.alcoholicas = await this.data.getData();
   }


}
