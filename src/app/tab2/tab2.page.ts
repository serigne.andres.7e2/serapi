import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { Drink2 } from '../interficies/interficies';
import { DetalleComponent } from '../components/detalle/detalle.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  noalcoholicas : Drink2[]=[];
  letras: any;

  constructor(private data: DataService,  private modalCtrl: ModalController) {}

  ngOnInit(): void{
    this.infoNoAlcoholicas();
  }

  infoNoAlcoholicas(event?){
    this.data.getNoAlcoholicas().subscribe(resp =>{
        this.noalcoholicas.push(...resp.drinks);
        this.letras = this.noalcoholicas;
        if(event){
          event.target.complete();
        }
    })
  }
  async verDetalle(id: string){
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: { id }
    })
    modal.present()
  }
 
  busca(nombre){
    if(nombre != ""){
      this.letras = this.noalcoholicas.filter(n => n.strDrink.toLowerCase().indexOf(nombre.toLowerCase())> -1);
    }
    else{
      this.letras = this.noalcoholicas;
    }
  }

  loadData(event){
    this.infoNoAlcoholicas(event);
  }
}