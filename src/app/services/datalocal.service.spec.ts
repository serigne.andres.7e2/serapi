import { TestBed } from '@angular/core/testing';
import { DataLocalService } from './datalocal.service';

describe('DatalocalService', () => {
  let service: DataLocalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataLocalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
