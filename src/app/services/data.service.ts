import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Alcoholicas, NoAlcoholicas, Detalle } from '../interficies/interficies';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { 
  }
  getAlcoholicas(){
    return this.http.get<Alcoholicas>('https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic')
  }

  getNoAlcoholicas(){
    return this.http.get<NoAlcoholicas>('https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic')
  }

  getDrinksDetalle(id){
    return this.http.get<Detalle>(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`)
  }

  
}
