import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Drink } from '../interficies/interficies';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  alcoholicas: Drink[]=[];
  private _storage: Storage | null = null;
  constructor(private storage: Storage) {
   this.init()
  }
  async init() {
   const storage = await this.storage.create();
   this._storage = storage;
  }

 async getData(){
  const data = await this.storage.get('alcoholicas')
  return data
 }

async addBebida(alcoholicas){
  this.alcoholicas.push(alcoholicas)
  this.storage.set('alcoholicas', this.alcoholicas)
  console.log(this.alcoholicas)
}

}